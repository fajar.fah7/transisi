<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
    		"name" 		=> "Administrator Transisi",
    		"email" 	=> "admin@transisi.id",
    		"password" 	=> Hash::make("transisi")
    	];
        DB::table('users')->insert($data);
    }
}
