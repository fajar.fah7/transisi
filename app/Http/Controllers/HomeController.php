<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;
use App\Employee;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data_company = Company::paginate(5);

        return view('home', ['data' => [
                'data_company'=>$data_company,
            ]
        ]);
    }

    public function employee(){
        $data_employee = Employee::select("employees.id", "employees.name", "employees.companies_id", "employees.email", "companies.name as companies_name")
        ->join("companies", "employees.companies_id", "companies.id")
        ->paginate(5);
        $data_list_company = Company::select("id", "name")->get();
        return view('employee', ['data' => [
                'data_employee'=>$data_employee,
                'data_list_company'=>$data_list_company,
            ]
        ]);
    }
}
