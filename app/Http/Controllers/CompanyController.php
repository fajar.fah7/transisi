<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use App\Company;
use Validator;

class CompanyController extends Controller
{
    public function Insert(Request $request){
    	$o = [];

    	$required = [
    		"company_name" => "required",
    		"company_email" => "required",
    		"company_website" => "required",
    		"company_logo" => "required|image|mimes:png|max:2048|dimensions:min_width=100,min_height=100",
    	];

    	$message = [
    		"company_name.required" => "Company name should be filled",
    		"company_email.required" => "Company email should be filled",
    		"company_website.required" => "Company website should be filled",
    		"company_logo.required" => "Company logo should be filled",
    		"company_logo.dimensions" => "Minimum company logo should 100x100 px",
    	];

    	$validator = Validator::make($request->all(), $required, $message);

    	if ($validator->fails()) {
    		$o["status"] = false;
    		$o["messages"] = $validator->errors()->all();
    		return $o;
    	} else {

    		$image 		= $request->file("company_logo");
    		$name 		= $image->getFilename();
    		$ext 		= $image->getClientOriginalExtension();
    		$newName 	= $name.".".$ext;
    		Storage::disk('public')->put($newName, File::get($image));

    		$company_name = $request->input("company_name");
    		$company_email = $request->input("company_email");
    		$company_website = $request->input("company_website");
    		$company_logo = $newName;

    		$data = [
    			"name"=>$company_name,
    			"email"=>$company_email,
    			"website"=>$company_website,
    			"logo"=>$company_logo,
    		];

    		$insert = Company::insert($data);
    		if (!$insert) {
    			$o["status"] = false;
    			$o["messages"][] = "Failed store data";
    			return $o;
    		} else {
    			$o["status"] = true;
    			$o["messages"][] = "Success Insert New Company";
    			return $o;
    		}
    	}
    }

    public function Update(Request $request){
    	$o = [];

    	$required = [
    		"company_name" => "required",
    		"company_email" => "required",
    		"company_website" => "required",
    	];

    	$message = [
    		"company_name.required" => "Company name should be filled",
    		"company_email.required" => "Company email should be filled",
    		"company_website.required" => "Company website should be filled",
    	];

    	$validator = Validator::make($request->all(), $required, $message);

    	if ($validator->fails()) {
    		$o["status"] = false;
    		$o["messages"] = $validator->errors()->all();
    		return $o;
    	} else {
	    	if ($request->file("company_logo") !== null) {

				$ruleLogo = ["company_logo" => "image|mimes:png|max:2048|dimensions:min_width=100,min_height=100"];

				$validationLogo = Validator::make([$request->file("company_logo")], $ruleLogo);
				
				if ($validationLogo->passes()) {

					$image 		= $request->file("company_logo");
					$name 		= $image->getFilename();
					$ext 		= $image->getClientOriginalExtension();
					$newName 	= $name.".".$ext;
					Storage::disk('public')->put($newName, File::get($image));

					$oldLogo = Company::where("id", $request->input("id_company"))
					->select("logo")
					->pluck("logo")
					->first();

					Storage::disk('public')->delete($oldLogo);

					$data = [
						"name"=>$request->input("company_name"),
						"email"=>$request->input("company_email"),
						"website"=>$request->input("company_website"),
						"logo"=>$newName,
					];

					$update = Company::where("id", $request->input("id_company"))->update($data);

					if (!$update) {
						$o["status"]  = false;
						$o["messages"][] = "Failed Update Data";
						return $o;
					} else {
						$o["status"]  = true;
						$o["messages"][] = "Success Update Data";
						return $o;
					}

				} else {

					$o["status"]  = false;
					$o["message"][] = "Gambar tidak sesuai";
					return $o;

				}
			} else {
				$data = [
					"name"=>$request->input("company_name"),
					"email"=>$request->input("company_email"),
					"website"=>$request->input("company_website"),
				];

				$update = Company::where("id", $request->input("id_company"))->update($data);

				if (!$update) {
					$o["status"]  = false;
					$o["messages"][] = "Failed Update Data";
					return $o;
				} else {
					$o["status"]  = true;
					$o["messages"][] = "Success Update Data";
					return $o;
				}
			}
    	}
    }

    public function Delete(Request $request){
    	$company_logo = Company::select("logo")
    	->pluck("logo")
    	->where("id", $request->input("id_company"))
    	->first();

    	Storage::disk('public')->delete($company_logo);

    	$delete = Company::where("id", $request->input("id_company"))
    	->delete();
    	if (!$delete) {
    		$o["status"] = false;
    		$o["messages"][] = "Failed Delete Data";
    		return $o;
    	} else {
    		$o["status"] = true;
    		$o["messages"][] = "Success Delete Data";
    		return $o;
    	}
    }
}
