<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use Validator;

class EmployeeController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
    public function Insert(Request $request){
    	$o = [];

    	$required = [
    		"employee_name" => "required",
    		"employee_company" => "required",
    		"employee_email" => "required",
    	];

    	$message = [
    		"employee_name.required" => "Employee name should be filled",
    		"employee_company.required" => "Employee company should be filled",
    		"employee_email.required" => "Employee email should be filled",
    	];

    	$validator = Validator::make($request->all(), $required, $message);

    	if ($validator->fails()) {
    		$o["status"] = false;
    		$o["messages"] = $validator->errors()->all();
    		return $o;
    	} else {

    		$data = [
    			"name"=>$request->input("employee_name"),
    			"companies_id"=>$request->input("employee_company"),
    			"email"=>$request->input("employee_email"),
    		];

    		$insert = Employee::insert($data);
    		if (!$insert) {
    			$o["status"] = false;
    			$o["messages"][] = "Failed store data";
    			return $o;
    		} else {
    			$o["status"] = true;
    			$o["messages"][] = "Success Insert New Employee";
    			return $o;
    		}
    	}
    }

    public function Update(Request $request){
    	$o = [];

    	$required = [
    		"id_employee" => "required",
    		"employee_name" => "required",
    		"employee_company" => "required",
    		"employee_email" => "required",
    	];

    	$message = [
    		"id_employee.required" => "Id Employee Should be filled",
    		"employee_name.required" => "Employee name should be filled",
    		"employee_company.required" => "Employee company should be filled",
    		"employee_email.required" => "Employee email should be filled",
    	];

    	$validator = Validator::make($request->all(), $required, $message);

    	if ($validator->fails()) {
    		$o["status"] = false;
    		$o["messages"] = $validator->errors()->all();
    		return $o;
    	} else {

    		$data = [
    			"name"=>$request->input("employee_name"),
    			"companies_id"=>$request->input("employee_company"),
    			"email"=>$request->input("employee_email"),
    		];

    		$update = Employee::where("id",$request->input("id_employee"))->update($data);
    		if (!$update) {
    			$o["status"] = false;
    			$o["messages"][] = "Failed update data";
    			return $o;
    		} else {
    			$o["status"] = true;
    			$o["messages"][] = "Success Update Employee";
    			return $o;
    		}
    	}
    }

    public function Delete(Request $request){
    	$id_employee = $request->input("id_employee");
    	$delete = Employee::where("id", $id_employee)->delete();
    	if (!$delete) {
    		$o["status"] = false;
			$o["messages"][] = "Failed delete data";
			return $o;
    	} else {
    		$o["status"] = true;
			$o["messages"][] = "Success delete data";
			return $o;
    	}
    }
}
