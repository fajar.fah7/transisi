<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get("/employee", "HomeController@employee");

Route::group(['prefix' => 'company'], function() {
    Route::post('insert', "CompanyController@Insert");
    Route::post('update', "CompanyController@Update");
    Route::post('delete', "CompanyController@Delete");
});

Route::group(['prefix' => 'employee'], function() {
    Route::post('insert', "EmployeeController@Insert");
    Route::post('update', "EmployeeController@Update");
    Route::post('delete', "EmployeeController@Delete");
});
