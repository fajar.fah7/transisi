@extends('layouts.app')

@section('content')

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="employee-preview-image">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="text-center">
                <img id="employee-image" style="max-width: 500px; max-height: 500px;">
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="employee-add-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="POST" id="employee-form">
                {{ csrf_field() }}
                <div class="card-body">
                    <div class="col-md-12">
                        <div class="text-center">
                            <h5 id="employee-title-form">Form Add Employee</h5>
                            <hr>
                        </div>
                    </div>
                    <div id="employee-messages-content"></div>
                    <div class="form-group row">
                        <label class="col-md-4">Employee Name</label>
                        <div class="col-md-8">
                            <input type="text" name="employee_name" id="employee_name" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-4">Company</label>
                        <div class="col-md-8">
                            <select class="form-control" name="employee_company" id="employee_company">
                                @foreach($data["data_list_company"] as $companies)
                                <option value="{{$companies->id}}">{{$companies->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-4">E-mail</label>
                        <div class="col-md-8">
                            <input type="text" name="employee_email" id="employee_email" class="form-control">
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="text-right">
                            <button type="button" class="btn btn-danger" id="employee-cancel">Cancel</button>
                            <button type="submit" class="btn btn-primary" id="employee-save" action="create">Save</button>
                        </div>
                    </div>
                    
                </div>
            </form>
        </div>
    </div>
</div>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link" id="employee-index-tab" href="{{ url('home') }}" role="tab" aria-controls="employee-index" aria-selected="true">Company</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" id="employee-add-tab" data-toggle="tab" href="#employee-add" role="tab" aria-controls="employee-add" aria-selected="false">Employee</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="employee-index" role="tabpanel" aria-labelledby="home-tab">
                            <div class="col-md-12">
                                <div class="text-right" style="margin-top: 10px;">
                                    <button type="button" id="employee-add-btn" class="btn btn-success">+ Add Employee</button>
                                </div>
                            </div>
                            <table class="table" style="margin-top: 10px;">
                                <tr>
                                    <th>Employee Name</th>
                                    <th>Company</th>
                                    <th>Email</th>
                                    <th>Action</th>
                                </tr>

                                @foreach ($data["data_employee"] as $employee)
                                
                                <tr>
                                    <td>{{ $employee->name }}</td>
                                    <td>{{ $employee->companies_name }}</td>
                                    <td>{{ $employee->email }}</td>
                                    <td>
                                        <button 
                                        id_employee="{{ $employee->id }}" 
                                        name_employee="{{ $employee->name }}" 
                                        companies_employee="{{ $employee->companies_id }}" 
                                        email_employee="{{ $employee->email }}" 
                                        companies_name="{{ $employee->companies_name }}" 
                                        onclick='editEmployee(this)' 
                                        class="btn btn-sm btn-success" 
                                        >Edit</button>
                                        <button 
                                        class="btn btn-sm btn-danger" 
                                        onclick='deleteEmployee("{{ $employee->id }}")' 
                                        >Delete</button>
                                    </td>
                                </tr>
                                
                                @endforeach
                            </table>
                            {{ $data["data_employee"]->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section("js")
    <script src="{{ asset('js/jquery.min.js') }}"></script>

    <script type="text/javascript">
        var id_employee

        $(document).ready(function(){

            $("#employee-add-btn").on("click", function(){
                $("#employee-add-modal").modal("show")
                employeeResetForm()
            })

            $("#employee-form").on("submit", function(e){
                e.preventDefault()
                var url;
                var action = $("#employee-save").attr("action")

                var data = new FormData(this)
                

                if (action === "create") {
                    url = "{{ url('/employee/insert') }}"
                } else if (action === "update") {
                    url = "{{ url('/employee/update') }}"
                    data.append("id_employee", id_employee)
                }

                $.ajax({
                    url:url,
                    method:"POST",
                    data:data,
                    dataType:"JSON",
                    processData: false,
                    contentType:false,
                    complete:function(res){
                        console.log(res)
                        if (res.responseJSON.status !== undefined) {
                            if (res.responseJSON.status === true) {
                                var successMessage = ""
                                $.each(res.responseJSON.messages, (k, v)=>{
                                    successMessage = successMessage+"<div class='alert alert-success text-center' role='alert'>"+v+"</div>"
                                })
                                $("#employee-messages-content").html(successMessage)
                                setTimeout(location.reload.bind(location), 300);
                            } else {
                                var errorMessage = ""
                                $.each(res.responseJSON.messages, (k, v)=>{
                                    errorMessage = errorMessage+"<div class='alert alert-warning' role='alert'>"+v+"</div>"
                                })
                                $("#employee-messages-content").html(errorMessage)
                            }
                        } else {
                            var errorMessage = ""
                            errorMessage = errorMessage+"<div class='alert alert-danger text-center' role='alert'>Terjadi kesalahan di server</div>"
                            $("#employee-messages-content").html(errorMessage)
                        }
                    }
                })
            })

            $("#employee-cancel").on("click", function(){
                employeeResetForm()
                $("#employee-add-modal").modal("hide")
            })

        })

        function editEmployee(attr){
            id_employee = attr.attributes[0].value
            var name_employee = attr.attributes[1].value
            var company_employee = attr.attributes[2].value
            var email_employee = attr.attributes[3].value
            var company_name = attr.attributes[4].value

            $("#employee-add-modal").modal("show")

            $("#employee_name").val(name_employee)
            $("#employee_company").val(company_employee)
            $("#employee_email").val(email_employee)
            $("#employee-save").attr("action", "update")
            $("#employee-save").html("Update")
            $("#employee-title-form").html("Form Update Employee")
        }

        function deleteEmployee(id_employee){
            var deleteData = false
            if (confirm("Are you sure delete data ?")) {
                deleteData = true
            } else {
                deleteData = false
            }

            $.ajax({
                url:"{{ url('/employee/delete') }}",
                method:"POST",
                data:{_token:"{{ csrf_token() }}",id_employee:id_employee},
                complete:function(res){
                    if (res.responseJSON.status === true) {
                        setTimeout(location.reload.bind(location), 300);
                    } else {
                        alert("Failed Delete Data")
                    }
                }
            })
        }

        function employeeResetForm(){
            $("#employee_name").val("")
            $("#employee_company").val("")
            $("#employee_email").val("")
            $("#employee-save").attr("action", "create")
            $("#employee-save").html("Save")
            $("#employee-title-form").html("Form Add Employee")
        }

    </script>

@endsection
