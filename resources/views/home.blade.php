@extends('layouts.app')

@section('content')

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="company-preview-image">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="text-center">
                <img id="company-image" style="max-width: 500px; max-height: 500px;">
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="company-add-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="POST" enctype="multipart/form-data" id="company-form">
                {{ csrf_field() }}
                <div class="card-body">
                    <div class="col-md-12">
                        <div class="text-center">
                            <h5 id="company-title-form">Form Add Company</h5>
                            <hr>
                        </div>
                    </div>
                    <div id="company-messages-content"></div>
                    <div class="form-group row">
                        <label class="col-md-4">Company Name</label>
                        <div class="col-md-8">
                            <input type="text" name="company_name" id="company_name" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-4">E-mail</label>
                        <div class="col-md-8">
                            <input type="text" name="company_email" id="company_email" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-4">Website</label>
                        <div class="col-md-8">
                            <input type="text" name="company_website" id="company_website" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-4">Logo</label>
                        <div class="col-md-8">
                            <input type="file" name="company_logo" id="company_logo" class="form-control-file">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="text-right">
                            <button type="button" class="btn btn-danger" id="company-cancel">Cancel</button>
                            <button type="submit" class="btn btn-primary" id="company-save" action="create">Save</button>
                        </div>
                    </div>
                    
                </div>
            </form>
        </div>
    </div>
</div>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="company-index-tab" data-toggle="tab" href="#company-index" role="tab" aria-controls="company-index" aria-selected="true">Company</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="employee-add-tab" href="{{ url('/employee') }}" role="tab" aria-controls="employee-add" aria-selected="false">Employee</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="company-index" role="tabpanel" aria-labelledby="home-tab">
                            <div class="col-md-12">
                                <div class="text-right" style="margin-top: 10px;">
                                    <button type="button" id="company-add-btn" class="btn btn-success">+ Add Company</button>
                                </div>
                            </div>
                            <table class="table" style="margin-top: 10px;">
                                <tr>
                                    <th>Company Name</th>
                                    <th>Email</th>
                                    <th>Website</th>
                                    <th>Logo</th>
                                    <th>Action</th>
                                </tr>

                                @foreach ($data["data_company"] as $company)
                                
                                <tr>
                                    <td>{{ $company->name }}</td>
                                    <td>{{ $company->email }}</td>
                                    <td>{{ $company->Website }}</td>
                                    {{-- <td>{{ $company->logo }}</td> --}}
                                    <td>
                                        <button class="btn btn-sm btn-primary" id="company-view-logo" onclick='previewImage("{{ $company->logo }}")' logo_name="{{ $company->logo }}">View</button>
                                    </td>
                                    <td>
                                        <button 
                                        id_company="{{ $company->id }}" 
                                        name_company="{{ $company->name }}" 
                                        email_company="{{ $company->email }}" 
                                        website_company="{{ $company->website }}" 
                                        onclick='editCompany(this)' 
                                        class="btn btn-sm btn-success" 
                                        >Edit</button>
                                        <button 
                                        class="btn btn-sm btn-danger" 
                                        onclick='deleteCompany("{{ $company->id }}")' 
                                        >Delete</button>
                                    </td>
                                </tr>
                                
                                @endforeach
                            </table>
                            {{ $data["data_company"]->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section("js")
    <script src="{{ asset('js/jquery.min.js') }}"></script>

    <script type="text/javascript">
        var id_company

        $(document).ready(function(){

            $("#company-add-btn").on("click", function(){
                $("#company-add-modal").modal("show")
                companyResetForm()
            })

            $("#company-form").on("submit", function(e){
                e.preventDefault()
                var url;
                var action = $("#company-save").attr("action")

                var data = new FormData(this)
                

                if (action === "create") {
                    url = "{{ url('/company/insert') }}"
                } else if (action === "update") {
                    url = "{{ url('/company/update') }}"
                    data.append("id_company", id_company)
                }

                $.ajax({
                    url:url,
                    method:"POST",
                    data:data,
                    dataType:"JSON",
                    processData: false,
                    contentType:false,
                    complete:function(res){
                        console.log(res)
                        if (res.responseJSON.status !== undefined) {
                            if (res.responseJSON.status === true) {
                                var successMessage = ""
                                $.each(res.responseJSON.messages, (k, v)=>{
                                    successMessage = successMessage+"<div class='alert alert-success text-center' role='alert'>"+v+"</div>"
                                })
                                $("#company-messages-content").html(successMessage)
                                // $("#company-add-modal").modal("hide")
                                setTimeout(location.reload.bind(location), 300);
                            } else {
                                var errorMessage = ""
                                $.each(res.responseJSON.messages, (k, v)=>{
                                    errorMessage = errorMessage+"<div class='alert alert-warning' role='alert'>"+v+"</div>"
                                })
                                $("#company-messages-content").html(errorMessage)
                            }
                        } else {
                            var errorMessage = ""
                            errorMessage = errorMessage+"<div class='alert alert-danger text-center' role='alert'>Terjadi kesalahan di server</div>"
                            $("#company-messages-content").html(errorMessage)
                        }
                    }
                })
            })

            $("#company-cancel").on("click", function(){
                companyResetForm()
                $("#company-add-modal").modal("hide")
            })

        })

        function previewImage(par){
            $("#company-preview-image").modal("show");
            $("#company-image").attr("src", "{{ url('storage/company_logo/') }}"+"/"+par)
        }

        function editCompany(attr){
            id_company = attr.attributes[0].value
            var name_company = attr.attributes[1].value
            var email_company = attr.attributes[2].value
            var website_company = attr.attributes[3].value

            $("#company-add-modal").modal("show")

            $("#company_name").val(name_company)
            $("#company_email").val(email_company)
            $("#company_website").val(website_company)
            $("#company-save").attr("action", "update")
            $("#company-save").html("Update")
            $("#company-title-form").html("Form Update Company")

            console.log(id_company)
        }

        function deleteCompany(id_company){
            var deleteData = false
            if (confirm("Are you sure delete data ?")) {
                deleteData = true
            } else {
                deleteData = false
            }

            $.ajax({
                url:"{{ url('/company/delete') }}",
                method:"POST",
                data:{_token:"{{ csrf_token() }}",id_company:id_company},
                complete:function(res){
                    if (res.responseJSON.status === true) {
                        setTimeout(location.reload.bind(location), 300);
                    } else {
                        alert("Failed Delete Data")
                    }
                }
            })
        }

        function companyResetForm(){
            $("#company_name").val("")
            $("#company_email").val("")
            $("#company_website").val("")
            $("#company_logo").val("")
            $("#company-save").attr("action", "create")
            $("#company-save").html("Save")
            $("#company-title-form").html("Form Add Company")
        }

    </script>

@endsection
